public enum Tile{
	BLANK("_"), 
	WALL("w"),
	HIDDEN_WALL("_"),
	CASTLE("C");
	
	private String name;
	
	private Tile(String name){
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
}