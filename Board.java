import java.util.Random;

public class Board{
	private Tile[][] grid;
	public int size;
	private Random rng;
	
	public Board(int size) {
		this.rng = new Random();
		this.size = size;
		grid = new Tile[size][size];
		
		for (int i = 0; i<grid.length; i++) {
			int randNum = rng.nextInt(size);
			for (int j = 0; j<grid[i].length; j++) {
				grid[i][j] = Tile.BLANK;
			}
			grid[i][randNum] = Tile.HIDDEN_WALL;
		}
	}
	
	public String toString(){
		String output = "";
		for (int i = 0; i < this.grid.length; i++) {
			
			for (int j = 0; j < this.grid.length; j++) {
				output += this.grid[i][j].getName() + " ";
			}
			output += "\n";
		}
		return output;
	}
	
	public int placeToken(int rows, int column) {
		if(rows >= this.size || rows < 0 || column >= this.size || column < 0){
			return -2;
		}
		if(grid[rows][column].equals(Tile.CASTLE) || grid[rows][column].equals(Tile.WALL)){
			return -1;
		}
		if(grid[rows][column].equals(Tile.HIDDEN_WALL)){
			grid[rows][column] = Tile.WALL;
			return 1;
		}
		grid[rows][column] = Tile.CASTLE;
		return 0;
	}
	
}