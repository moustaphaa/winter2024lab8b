import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Board test = new Board(5);
		
		System.out.println("Welcome to the board game!");
		
		int numCastles = 7;
		int turns = 0;
		//loop for the game
		while(numCastles > 0 && turns < 8){
			System.out.println(test);
			System.out.println("Number of castles left: " + numCastles);
			System.out.println("Number of turns: " + turns);
			System.out.println("--------------------------");
			
		
			System.out.println("Choose a row:");
			int row = Integer.parseInt(reader.nextLine());
		
			System.out.println("Choose a column:");
			int column = Integer.parseInt(reader.nextLine());
			
			int tokenReturn = test.placeToken(row, column);
			//loop to see if the row and column are valid. If not, re-enter
			while(tokenReturn < 0){
				System.out.println("Sorry. Re-enter a row and a column");
				
				System.out.println("Choose a VALID row:");
				row = Integer.parseInt(reader.nextLine());
			
				System.out.println("Choose a VALID column:");
				column = Integer.parseInt(reader.nextLine());
				
				tokenReturn = test.placeToken(row, column);
			
			}
			//if there is a wall there
			if(tokenReturn == 1){
				System.out.println("There was unfortunately a wall in that position :(");
				turns ++;
			}
			//if there is not a wall
			if(tokenReturn == 0){
				System.out.println("The castle was successfully placed! :)");
				turns ++;
				numCastles --;
			}
		}
		System.out.println(test);
		
		if(numCastles == 0){
			System.out.println("Congratulations! You won");
		}
		else{
			System.out.println("You trash lol");
		}
	
		
	}
}